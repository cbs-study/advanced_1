"""Декоратор, який буде заміряти час виконання для наданої функції"""

import time


def timer(func):
    def wrapper(*args, **kwargs):
        start = time.time()
        result = func(*args, **kwargs)
        end = time.time()
        print(
            f"The {func.__name__} of the function execution time is {end - start} seconds."
        )
        return result

    return wrapper


# Декоратор, який вимірює час виконання функції
@timer
def countdown(seconds):
    while seconds > 0:
        print(f"{seconds} seconds left.")
        time.sleep(1)
        seconds -= 1
    print("Time is up!")


countdown(3)
